import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList";
import { Route, NavLink } from "react-router-dom";

class App extends Component {
  state = {
    todos: todosList,
    value: ""
  };

  handleChange = event => {
    this.setState({ value: event.target.value });
  };

  handleCreateTodo = event => {
    if (event.key === "Enter") {
      const newTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 1000000000),
        title: this.state.value,
        completed: false
      };
      // immutability
      // create a copy
      const newTodos = this.state.todos.slice();
      // modify a copy
      newTodos.push(newTodo);
      // overwrite original with a copy
      this.setState({ todos: newTodos, value: " " });
    }
  };

  handleDeleteTodo = (event, todoIdToDelete) => {
    //.filter
    const newTodos = this.state.todos.filter(todo => {
      if (todo.id === todoIdToDelete) {
        return false;
      } else {
        return true;
      }
    });
    this.setState({ todos: newTodos });
  };

  // handleCompleted = todoIdToToggle => event => {
  //   const newTodos = this.state.todos.map(todo => {
  //     if (todo.completed === todoIdToToggle) {
  //       if (todo.completed === true) {
  //         todo.completed = false;
  //       } else {
  //         todo.completed = true;
  //       }
  //     }
  //     return todo;
  //   });
  //   this.setState({ todos: newTodos });
  // };

  handleCompleted = id => {
    let { todos } = this.state;
    let { completed } = todos[id];
    todos[id].completed = !completed;
    console.log(todos);
    this.setState({ todos });
  };

  handleAllCompleted = () => {
    const newTodos = this.state.todos.filter(todo => {
      if (todo.completed === true) {
        return false;
      } else {
        return true;
      }
    });
    this.setState({ todos: newTodos });
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            value={this.state.value}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route
          exact
          path="/"
          render={() => (
            <TodoList
              handleDeleteTodo={this.handleDeleteTodo}
              todos={this.state.todos}
              handleCompleted={this.handleCompleted.bind(this)}
              handleAllCompleted={this.handleAllCompleted}
            />
          )}
        />
        <Route
          exact
          path="/completed"
          render={() => (
            <TodoList
              handleDeleteTodo={this.handleDeleteTodo}
              todos={this.state.todos.filter(todo => todo.completed === true)}
              handleCompleted={this.handleCompleted.bind(this)}
              handleAllCompleted={this.handleAllCompleted}
            />
          )}
        />
        <Route
          exact
          path="/active"
          render={() => (
            <TodoList
              handleDeleteTodo={this.handleDeleteTodo}
              todos={this.state.todos.filter(todo => todo.completed !== true)}
              handleCompleted={this.handleCompleted.bind(this)}
              handleAllCompleted={this.handleAllCompleted}
            />
          )}
        />
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>
              {(() => {
                let incomplete = this.state.todos.filter(
                  todo => todo.completed !== true
                );
                return incomplete.length;
              })()}
            </strong>{" "}
            item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

// class TodoList extends Component {
//   render() {
//     return (
//       <section className="main">
//         <ul className="todo-list">
//           {this.props.todos.map((todo, index) => (
//             <TodoItem
//               id={index}
//               handleDeleteTodo={event =>
//                 this.props.handleDeleteTodo(event, todo.id)
//               }
//               title={todo.title}
//               completed={todo.completed}
//               handleCompleted={this.props.handleCompleted}
//               handleAllCompleted={() => this.props.handleAllCompleted}
//             />
//           ))}
//         </ul>
//       </section>
//     );
//   }
// }

export default App;
