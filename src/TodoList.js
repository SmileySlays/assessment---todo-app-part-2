import React, { Component } from "react";
import TodoItem from "./TodoItem";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo, index) => (
            <TodoItem
              id={index}
              handleDeleteTodo={event =>
                this.props.handleDeleteTodo(event, todo.id)
              }
              title={todo.title}
              completed={todo.completed}
              handleCompleted={this.props.handleCompleted}
              handleAllCompleted={() => this.props.handleAllCompleted}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default TodoList;
