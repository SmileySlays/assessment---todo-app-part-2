import React, { Component } from "react";

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            defaultChecked={this.props.completed}
            onChange={this.props.handleCompleted.bind(this, this.props.id)}
          />
          <label>{this.props.title}</label>
          <button
            className="destroy"
            onClick={this.props.handleDeleteTodo.bind(this)}
          />
        </div>
      </li>
    );
  }
}

export default TodoItem;
